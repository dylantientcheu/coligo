import { Component, OnInit } from '@angular/core';
import { ContactHttpService } from '../contact-http.service';
import { FormGroup, FormBuilder } from '@angular/forms';

// tslint:disable: no-redundant-jsdoc

interface Response {
  message?: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  addFlip = false;
  filtered = false;
  contacts = [];
  loading = false;
  filteredContacts = [];
  ogContacts = [];
  itemsCopy = [];
  searchModel = '';
  selectedContact = {
    name: '',
    phone: '',
    email: '',
    favorite: false,
  };


  contactForm = this.formBuilder.group({ // add contact form
    name: [''],
    phone: [''],
    email: [''],
    address: [''],
    company: [''],
    picture: [''],
    favorite: false,
  });

  constructor(protected ContactsService: ContactHttpService, private formBuilder: FormBuilder) {
    this.getContacts();
  }

  ngOnInit() {
  }

  /**
   * Highlight a selected contact
   *
   * @param {*} item the details about the contact selected
   * @memberof HomeComponent
   */
  selectContact(item): void {
    this.selectedContact = {
      name: item.name,
      phone: item.phone,
      email: item.email,
      favorite: item.favorite,
    };
  }

  /**
   * Get contacts from server
   *
   * @memberof HomeComponent
   */
  getContacts() {
    this.loading = true;
    this.ContactsService.getContacts().subscribe(data => {
      this.loading = false;
      this.contacts = data;
    });
  }

  /**
   * Delete contacts
   *
   * @param {*} id user's Id
   * @memberof HomeComponent
   */
  deleteContact(id): void {
    this.loading = true;

    this.ContactsService.deleteContact(id).subscribe((data: Response) => {
      if (data.message === 'Record removed.') {
        console.log(id, 'Deleted!');
        this.loading = false;
        this.getContacts();
      }
    });
  }

  /**
   * fav a contact
   *
   * @param {*} id id of cont
   * @param {*} item bory
   * @memberof HomeComponent
   */
  favContact(id, item): void {
    this.loading = true;
    item.favorite ? item.favorite = false : item.favorite = true;
    this.ContactsService.updateContact(id, item).subscribe((data: Response) => {
      if (data.message === 'Record updated.') {
        console.log(id, 'Updated!');
        this.loading = false;
      }
    });
  }

  /**
   * create a contact
   *
   * @memberof HomeComponent
   */
  createContact(): void {
    this.loading = true;
    this.ContactsService.addContacts(this.contactForm.value).subscribe(data => {
      if (data._createdOn) {
        this.loading = false;
        this.addFlip = false;
        this.getContacts();
      }
    });
  }

  filterFavs(): void {
    this.ogContacts = this.contacts;
    this.contacts = this.contacts.filter(contact => contact.favorite);
    this.filtered = true; // change the filter button accordingly
  }

  unfilterFavs(): void {
    this.contacts = this.ogContacts;
    this.filtered = false; // change the filter button accordingly
  }

  updateSearchModel(value) {
    this.searchModel = value;
  }

}
