import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { ContactHttpService } from '../contact-http.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loading = false;
  error = false;
  logger = {
    email: '',
    password: '',
  };

  constructor(private router: Router, private appComponent: AppComponent, private user: ContactHttpService) { }

  ngOnInit() {
  }

  goHome() {
    this.loading = true;
    if (this.user.userLogin.email === this.logger.email && this.user.userLogin.password === this.logger.password) {
      setTimeout(() => {
        this.loading = false;
        this.error = false;
        this.appComponent.loggedIn = true;
        this.router.navigate(['/home']);
      }, 1500);
    } else if (this.user.adminLogin.email === this.logger.email && this.user.adminLogin.password === this.logger.password) {
      setTimeout(() => {
        this.loading = false;
        this.error = false;
        this.appComponent.loggedIn = true;
        this.router.navigate(['/admin']);
      }, 1500);
    } else {
      this.loading = false;
      this.error = true;
    }
  }

}
