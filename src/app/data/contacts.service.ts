import { Injectable } from '@angular/core';

@Injectable()

export class ContactsDataService {
  data = [
    {
      picture: '',
      name: 'Johanna Browning',
      company: 'NURPLEX',
      email: 'johannabrning@nurplex.name',
      favorite: false,
      phone: '+237 804 454 289',
      address: '305 Perry Terrace, Chapin, South Dakota, 7030'
    },
    {
      picture: '',
      name: 'Haley Alvarado',
      company: 'NAXDIS',
      email: 'haleyalrado@naxdis.io',
      favorite: false,
      phone: '+237 915 544 345',
      address: '819 John Street, Cartwright, Northern Mariana Islands, 1506'
    },
    {
      picture: '',
      name: 'Gabrielle Rush',
      company: 'SYBIXTEX',
      email: 'gabrieeush@sybixtex.ca',
      favorite: false,
      phone: '+237 833 443 341',
      address: '679 Adams Street, Martinez, Virginia, 8668'
    },
    {
      picture: '',
      name: 'Langley Mcintosh',
      company: 'QUILK',
      email: 'langleymctosh@quilk.org',
      favorite: false,
      phone: '+237 834 502 392',
      address: '861 Willoughby Street, Orviston, Washington, 7003'
    },
    {
      picture: '',
      name: 'Glenda Cotton',
      company: 'NETBOOK',
      email: 'glendtton@netbook.info',
      favorite: true,
      phone: '+237 857 586 294',
      address: '679 Tabor Court, Morgandale, Hawaii, 748'
    },
    {
      picture: '',
      name: 'Hope Moses',
      company: 'QUADEEBO',
      email: 'hoposes@quadeebo.us',
      favorite: false,
      phone: '+237 885 441 213',
      address: '104 Stillwell Place, Berlin, Georgia, 1642'
    },
    {
      picture: '',
      name: 'Chaney Pitts',
      company: 'OPTICALL',
      email: 'chaneyitts@opticall.biz',
      favorite: true,
      phone: '+237 863 529 321',
      address: '946 Willmohr Street, Chestnut, American Samoa, 5275'
    },
    {
      picture: '',
      name: 'Collins Powers',
      company: 'IZZBY',
      email: 'collinswers@izzby.co.uk',
      favorite: false,
      phone: '+237 916 552 312',
      address: '286 Stockton Street, Waikele, Idaho, 6866'
    },
    {
      picture: '',
      name: 'Kayla Wilkins',
      company: 'ISOTERNIA',
      email: 'kayla wkins@isoternia.biz',
      favorite: false,
      phone: '+237 875 574 286',
      address: '694 Willoughby Avenue, Albrightsville, Ohio, 4755'
    },
    {
      picture: '',
      name: 'Mclaughlin Haynes',
      company: 'ORBEAN',
      email: 'mclaughlin ynes@orbean.net',
      favorite: false,
      phone: '+237 852 452 219',
      address: '868 Navy Walk, Lopezo, California, 5072'
    },
    {
      picture: '',
      name: 'Pam Colon',
      company: 'LOVEPAD',
      email: 'pamolon@lovepad.me',
      favorite: true,
      phone: '+237 833 407 347',
      address: '702 College Place, Durham, Massachusetts, 3326'
    },
    {
      picture: '',
      name: 'Banks Battle',
      company: 'DIGITALUS',
      email: 'banks ttle@digitalus.com',
      favorite: true,
      phone: '+237 844 414 318',
      address: '834 Concord Street, Goochland, Kentucky, 1117'
    },
    {
      picture: '',
      name: 'Kaufman Pollard',
      company: 'OCTOCORE',
      email: 'kaufman plard@octocore.name',
      favorite: true,
      phone: '+237 859 519 308',
      address: '757 McKibben Street, Snowville, Illinois, 7573'
    },
    {
      picture: '',
      name: 'Janis Kinney',
      company: 'HELIXO',
      email: 'janis nney@helixo.io',
      favorite: true,
      phone: '+237 840 539 263',
      address: '381 Clara Street, Abrams, Alaska, 6826'
    },
    {
      picture: '',
      name: 'Cole Prince',
      company: 'ZOLARITY',
      email: 'cole ince@zolarity.ca',
      favorite: true,
      phone: '+237 965 418 273',
      address: '417 Commerce Street, Valmy, North Carolina, 8276'
    },
    {
      picture: '',
      name: 'Jeanine Love',
      company: 'KENEGY',
      email: 'jeaninlove@kenegy.org',
      favorite: false,
      phone: '+237 802 487 258',
      address: '746 Crescent Street, Woodlake, Maryland, 8890'
    },
    {
      picture: '',
      name: 'Young Mays',
      company: 'INTRAWEAR',
      email: 'younmays@intrawear.info',
      favorite: true,
      phone: '+237 803 560 260',
      address: '620 Stuyvesant Avenue, Montura, Connecticut, 9897'
    },
    {
      picture: '',
      name: 'Farmer Castillo',
      company: 'GROK',
      email: 'farmer caillo@grok.us',
      favorite: false,
      phone: '+237 961 474 339',
      address: '608 Adler Place, Welch, Oregon, 6154'
    },
    {
      picture: '',
      name: 'Miranda Garner',
      company: 'AQUASURE',
      email: 'miranda rner@aquasure.biz',
      favorite: false,
      phone: '+237 834 502 338',
      address: '315 Portland Avenue, Blairstown, Wyoming, 9851'
    },
    {
      picture: '',
      name: 'Burnett Houston',
      company: 'MINGA',
      email: 'burnett hston@minga.co.uk',
      favorite: true,
      phone: '+237 998 408 252',
      address: '215 Arlington Place, Coalmont, Minnesota, 9448'
    },
    {
      picture: '',
      name: 'Sherri Mccarty',
      company: 'ROBOID',
      email: 'sherrmarty@roboid.biz',
      favorite: true,
      phone: '+237 882 581 280',
      address: '783 Vandam Street, Klagetoh, South Carolina, 356'
    },
    {
      picture: '',
      name: 'Barnes Hardin',
      company: 'TECHADE',
      email: 'barnerdin@techade.net',
      favorite: true,
      phone: '+237 944 443 264',
      address: '504 Colonial Road, Romeville, New Hampshire, 2653'
    },
    {
      picture: '',
      name: 'Cortez Jimenez',
      company: 'GUSHKOOL',
      email: 'cortezenez@gushkool.me',
      favorite: true,
      phone: '+237 958 444 214',
      address: '436 Charles Place, Bluffview, Texas, 3040'
    },
    {
      picture: '',
      name: 'Judith Quinn',
      company: 'SNORUS',
      email: 'judithuinn@snorus.com',
      favorite: false,
      phone: '+237 928 593 289',
      address: '552 Lawn Court, Bison, Nebraska, 8531'
    },
    {
      picture: '',
      name: 'Gayle Nieves',
      company: 'PARCOE',
      email: 'gayleves@parcoe.name',
      favorite: false,
      phone: '+237 829 540 376',
      address: '634 Etna Street, Cawood, Montana, 1227'
    }
  ];

  getData() {
    return this.data;
  }


}
