import { Component } from '@angular/core';
import { ContactsDataService } from './data/contacts.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private ContactService: ContactsDataService, protected router: Router) {

  }
  title = 'coligo';
  loggedIn = false;

  logout() {
    this.router.navigate(['/auth']);
  }
}
